var app = require('./server');
var ds = app.dataSources.postgresql;

const CreateForeignKeys = (model, callback) => {
	const relations = model.settings.relations;
	const foreignkeys = Object.keys(relations);
	let idx = 0;
	for (key of foreignkeys){
		const table_from = model.name.toLowerCase();
		const table_to = relations[key].model.toLowerCase();
		const column_from = relations[key].foreignKey;
		const column_to = relations[key].primaryKey ? relations[key].primaryKey : "id";
		const sql = `ALTER TABLE "${table_from}" ADD CONSTRAINT "${table_from}_fk${idx}" FOREIGN KEY ("${column_from}") REFERENCES "${table_to}" ("${column_to}");`;
		model.dataSource.connector.query(
			sql,
			(error) => {
				if (error)
					console.log('Constraint not added: ' + error);
				else
					console.log("Constraint added: OK.")
			});

		idx += 1;
	}
}

new Promise((resolve, reject) => {
	app.dataSources.postgresql.autoupdate( () => {
		console.log("Tables have been created.")
		resolve();
	});
}).then(()=>{
	for (model of Object.keys(app.models)){
		console.log(`model: ${model}`);
		CreateForeignKeys(app.models[model]);
	}
})
